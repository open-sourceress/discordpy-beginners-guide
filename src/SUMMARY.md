# Summary

[Introduction](./intro.md)

- [Development Setup](./dev-setup/README.md)
  - [Installing Python](./dev-setup/python.md)
  - [Creating a Virtual Environment](./dev-setup/venv.md)
  - [Installing discord.py](./dev-setup/discordpy.md)
  - [Installing a Text Editor](./dev-setup/editor.md)
