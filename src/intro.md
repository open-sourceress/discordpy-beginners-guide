# Introduction

This guide will teach you how to write a Discord bot using the Python language and discord.py library. Along the way,
we'll cover topics such as virtual environments, decorators, and asynchronous programming. While version control is out
of scope of this guide, some tips for readers using Git will be included where relevant. By the end of this book, you
will have a Discord bot with a few commands, and be able to add more commands on your own.

## Prerequisites

No prior Python experience is required. This guide starts from scratch, before you even have Python installed. However,
there are already a plethora of good resources for learning the fundamentals of Python that could teach better than a
chapter in this guide. A list of those resources is included in chapter [TODO].

You will need a Discord account to start, but this guide will cover creating the bot account. Familiarity with Discord's
features (how to create a server, create channels, send messages, roles and permissions, etc) is strongly recommended.

## Contributing
If you spot a typo or error, or have an improvement to suggest, you can submit an [issue][issues] or
[merge request][merge requests] to <https://gitlab.com/open-sourceress/discordpy-beginners-guide/>.

[issues]: https://gitlab.com/open-sourceress/discordpy-beginners-guide/issues
[merge requests]: https://gitlab.com/open-sourceress/discordpy-beginners-guide/merge_requests
