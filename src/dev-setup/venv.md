# Creating a Virtual Environment

By default, Python installs packages like discord.py alongside itself, so Python projects anywhere on your computer can
use them. While this is convenient at first, it becomes unwieldy to install multiple versions of the same package if
different projects require different versions. The solution is virtual environments (a.k.a. virtualenvs or venvs):
per-project containers for the specific packages you need.

## Creating the Project Folder

First of all, we'll need a folder to store our bot's files, including its virtualenv. This folder should be somewhere
you can both read and create/write files, for example your user directory. If you will be developing your bot on
multiple computers, you can use a USB drive. Since we'll be using the command line, omitting spaces, quotes, and
apostrophes from the folder name will make things simpler.

> **Note for Git users:** this folder should be your repository root.

From here on, this folder will be referred to as the project folder, project directory, or project root. All of the
action related to your bot will happen here.

## Running `venv`

Python comes with a built-in program for creating virtual environments, called `venv`. To run it, first open a terminal
in your project folder.

> **Note:** on Windows, you can open the folder in Windows Explorer, shift-right-click an empty area, and click "Open
> command window here" to open a terminal in a specific folder quickly.

Next, we'll run the command to create our virtual environment. This will create a folder called `venv` where our
dependencies will go.

> **Note:** recall from the [previous section](./python.md#checking-your-installation) that `py` is a placeholder for
> your specific Python command. Depending on your OS and how you installed Python, this could be `py`, `py -3`,
> `python`, or `python3`.

```shell
$ py -m venv venv
```

This may take a few minutes. `venv` won't produce any text in your terminal, but will create the folder in your project
directory and set up an empty virtual environment inside.

Congratulations! You just ran your first Python file. There are a few parts to this command we'll take a look at:

- `py` (or your platform-specific equivalent) is an executable file that lives somewhere on your computer. In this
  case, it's the Python interpeter you installed earlier, which runs Python scripts.
- `-m` is a flag that tells Python that we will provide a module name rather than a path to a `.py` file. This way,
  rather than finding the script for `venv` ourselves, we make Python do it for us.
- The first `venv` is the name of the module we want Python to find. `venv` is part of the Python standard library,
  which means it's installed automatically when Python is installed.
- The second `venv`, and any arguments we passed afterward, are passed along to the `venv` script. For `venv`, the
  first argument is the name of the folder to create for the new virtual environment.

We'll use a similar command to install discord.py and then run our bot.

## Activating the Virtualenv

Finally, we need to tell Python to put our dependencies in this virtualenv, and to look for packages in this virtualenv
too. We do this by activating the virtualenv.

#### Windows

```shell
$ venv\Scripts\activate
```

#### Mac/Linux

```shell
$ source venv/bin/activate
```

You should now see `(venv)` before the prompt. This means you have a virtualenv activated.

> **Note:** when you open a new terminal, you will have to do this last step again.
>
> **Note:** if you need to deactivate your virtualenv, run `venv\Scripts\deactivate` on Windows, and `deactivate` on
> Mac/Linux.

Now that we have a virtual environment set up, let's install some packages into it!
