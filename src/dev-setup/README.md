# Development Setup

The first step is to set up our tools. To write a Discord bot you'll need

- A recent version of Python (3.6+)
- (Recommended) a virtual environment for our dependencies
- The latest version of discord.py (1.0.1 at the time of this writing)
- A text editor to write your bot in

We'll cover each of these in order.
