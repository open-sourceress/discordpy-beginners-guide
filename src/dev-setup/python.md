# Installing Python

Python is a simple, dynamically but strongly-typed, interpreted programming language. Its simple syntax makes it easy to
learn and write quickly, and its large collection of libraries makes it easy to get started in any area.

discord.py requires Python version 3.5.3 or higher, but 3.6+ is recommended. If you install the latest version, you
should be good to go.

## Windows

1. Go to <https://www.python.org/downloads/>.
1. To download the latest version, click the yellow download button at the top of the page.
1. Run the installer once it's finished downloading.
1. If you want a quick installation with the defaults for many options follow the directions below. Otherwise, skip to
   the next step.
   1. Check the "Add Python to PATH" checkbox at the bottom of the first screen.
   1. You can check the "Install launcher for all users" checkbox or leave it unchecked. This will make Python
      accessible from every user account, but requires admin access. Either way will work for this guide.
1. If you want to customize Python, click "Customize installation" and follow the directions below.
   1. Under "Optional Features", ensure "pip" is checked. The other options aren't relevant for this guide. Note that
      checking the "for all users" option next to "py launcher" will require admin access to install.
   1. Under "Advanced Options", ensure "Add Python to environment variables" is checked. The other options aren't
      relevant to this guide. Note that checking "Install for all users" will require admin access to install.
   1. Click "Install" and wait for installation to complete.

## Mac

TODO

## Linux

Python 3 is distributed in most package managers. Use your package manager to install the packages `python3` and `pip3`.
For example, on Ubuntu the command is `sudo apt-get install python3 pip3`.

## Checking your Installation

Once you've installed Python, check that it's installed correctly and accessible.

> **Note:** from here on in the guide, we will use `py` to represent the Python command, as it varies across platforms.
> - If you're on Windows and checked the "py launcher" option during installation, you can use this command as-is. If
    you run the commands below and get a version starting with 2, use `py -3` instead.
> - If you're on Windows and didn't check the "py launcher" option, replace `py` with `python`.
> - If you're not on Windows, replace `py` with `python3`.

1. Open a terminal.
   - On Windows, press Win-R to open the run dialog, and type `cmd` and press Enter to open Command Prompt.
   - On Mac and Linux, open the Terminal app.
1. Type `py --version` and press Enter.
   ```shell
   $ py --version
   Python 3.7.3
   ```
   If you get output similar to this, you've correctly installed Python correctly and are ready to move on.
